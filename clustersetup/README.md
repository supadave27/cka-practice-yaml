# Setting up a cluster with kubeadm

## Init
Can use something like this kubeadm init --cri-socket=/var/run/crio/crio.sock --apiserver-advertise-address=192.168.1.174  --pod-network-cidr=192.168.0.0/16
but I use crio so neeed to specify the cgroup, hence the kubeadm config in this directory. 
`kubeadm init  --config kubeadm-config.yaml`

## CNI
Using calico https://projectcalico.docs.tigera.io/getting-started/kubernetes/quickstart
Run `kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.1/manifests/tigera-operator.yaml` and `kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.1/manifests/custom-resources.yaml`

Should end up with this 
![image.png](./image.png)

## Joining some nodes
When you run kubeadm init above you will get a join command like so `kubeadm join 192.168.1.174:6443 --token 8a8hqa.jv8caenwrted14xr --discovery-token-ca-cert-hash sha256:d08b22e796522eb9042bae068915bd638b90cd446d25313b2c5662ada89781ec` or you can print one like with `kubeadm token create --print-join-command`
Run that command on the nodes you'd like to join (given you've install kubeadm, kubelet and your container runtime) - aka follow the kube doco https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/
Nodes sould come up like so 
![image-1.png](./image-1.png)
and all pods should be good like so
![image-2.png](./image-2.png)
