# Ansible Playbook for Resetting K8s Cluster 

# First thing setup ansible 
`sudo apt install ansible`
`sudo apt install sshpass`
Directory layout 
https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#directory-layout


# Running playbook 
ansible-playbook resetk8s.yml -i inventories/dev/hosts -k -K


# Randoms: thank you for your help
- https://medium.com/@desfocado/how-to-pass-variables-between-ansible-plays-and-hosts-adcf5dfa7439
