# Using externalName service for external service

- Public APIs https://documenter.getpostman.com/view/8854915/Szf7znEe
- Used http://openlibrary.org/search.json?q=the+lord+of+the+rings
- Can test in a nginx pod like so
-- curl -H "Host: openlibrary.org" --location --request GET 'http://my-service/search.json?q=the+lord+of+the+rings'
--- how good is that! I love k8s! 
- https://kubernetes.io/docs/concepts/services-networking/service/#externalname
